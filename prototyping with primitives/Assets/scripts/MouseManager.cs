﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseManager : MonoBehaviour
{
    //Know what objects are clickeable
    public layerMask clickeableLayer;

    public int publicÍntTest;

    //swap cursors per object 
    public texture2D pointer; //normal cursors
    public texture2D target; //for clickeable world object
    public texture2D doorway; //doorways
    public texture2D combat; //for combat actions

    // Update is called once per frame
    void Update()
    {
        raycasthit hit;
        if (physics.raycast(camera.main.screenpointToray(input.mousePosition), out hit, 50, clickeableLayer.value))
     {
         bool door = false;
         if (hit.collider.gameobject.tag == "doorway")
       {
     cursor.setcursor(doorway, new vector2(16, 16), cursormode.auto);
     door = true;

     }
      else
     {
          cursor.setcursor(target, new vector2(16, 16), cursormode.auto);
     }
     }
        else
        {
            cursor.setcursor(pointer, vector2.zero, cursormode.auto);
        }
        }
}
